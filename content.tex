\usepackage[utf8]{inputenc}
\usepackage{cancel}
\usepackage{acronym}
\usepackage{graphicx}
% \usepackage{keystroke}
\usepackage{verbatimbox}
\usepackage{fontawesome}
\usepackage{menukeys}
\usepackage{multicol}
\usepackage[outputdir=build]{minted}
\title{On SSH Keys}
\author{Krzysztof Zych}
\institute{Rebased}
\date{\today}

\newacro{PAM}[PAM]{Pluggable Authentication Module}
\begin{document}

  \maketitle
  \section*{Contents}

  \begin{frame}
  \tableofcontents
  \end{frame}

  \begin{frame}{What we use ssh for \hfill { \faQuestionCircle }}
    \begin{enumerate}
    \item secure transport: \texttt{git}, \texttt{mercurial}
    \item interactive shell (it's in the name: \textbf{S}ecure \textbf{SH}ell)
    \item secure file transfer: \texttt{scp, sftp, rsync}
    \item deployment: \texttt{capistrano, shipit, grunt}, Heroku 
    \item tunneling: TCP, X11, SOCKS
    \end{enumerate}
  \end{frame}

  \begin{frame}{Two elements of a secure connection \hfill { \faUserSecret }}
    \begin{center}
      \alt<1>{\Large Transport\\+\\Authentication}{\Large Encrypted TCP connection\\+\\Key negotiation}
    \end{center}
    \onslide<3>{
      \begin{alert}{NOTE \\}
        SSH doesn't do \textit{Authorization}, which is the underlying OS's responsibility. On all modern Unixes, this is done by \acs{PAM}. \\
        \hyperlink{pam}{\beamerbutton{Tell me about PAM}}
      \end{alert}
    }
    \note<3->{Which means that SSH imposes no rules of its own. Accounts are directly mapped to system usernames, and no additional permissions are granted beyond that of the target user.}
  \end{frame}

  \section{Asymmetric key cryptography}
    \begin{frame}{Back in the \xcancel{nineties} \hfill { \faCalendar }}
      \begin{itemize}
        \item Whitfield Diffie, Martin Hellman 1976
        \item Ralph Merkle 1974 (\textit{Merkle's Puzzles})
        \item Ron \textcolor{red}{R}ivest, Adi \textcolor{red}{S}hamir, Leonard \textcolor{red}{A}dleman 1977
        \item (EC) Neal Koblitz 1987, Victor S. Miller 1985
      \end{itemize}
      \begin{flushright}
        \hyperlink{keygen}{\beamerbutton{Boring math incoming, skip it}}
      \end{flushright}
      \note{
	      Merkle's puzzles are a bit like bitcoin works today - to estabilish a session, Alice sends a number of not-too-hard crypto puzzles, of the form \texttt{random identifier + symmetrical key}, all encrypted just enough to be bruteforceable. Then Bob picks one at random, cracks it, then uses symmetrical key found inside to encrypt his messages. He sends an encrypted message + that identifier (in plaintext) to Alice, who now knows (by the identifier) which key was used. Eavesdropper Eve can see the identifier, but has no way to map it to the secret key used without cracking all the puzzles first. \\
	      Eliptic Curve Cryptography relies on a different hard problem - a parametrized curve defined over the finite field. This talk is not about them.
      }
    \end{frame}

    \begin{frame}{Discrete maths\hfill1/3}
      \onslide<1->{
        \begin{equation}
          \tag{Modulo division}
          a = k \cdot b + \mathbf{q} \iff a \bmod b = q
        \end{equation} 
      }
      \only<1>{
        Example: $34 \bmod 10 = 4 \iff 34 = 3 \cdot 10 + 4$
      }
      \onslide<2->{
          \begin{equation}
            \tag{Set of integers modulo p}
            {\mathbb{Z}}_p = \lbrace 0, 1, 2, \ldots, p - 1 \rbrace
          \end{equation}
      }
      \only<2>{
        Example: $\mathbb{Z}_2 = \lbrace 0, 1 \rbrace
        \quad \mathbb{Z}_9 = \lbrace 0 \ldots 8 \rbrace$
      }
      \onslide<3->{
        \begin{equation}
          \tag{Congruence modulo p}
          x \equiv y \bmod z \iff x = k \cdot z + y
        \end{equation}
      }
      \only<3>{
        Example: $y=4$ and $z=10$, then $x = 4, 14, 24, \ldots$
      }
      \onslide<4->{
        \begin{equation}
          \tag{Multiplicative inverse}
          x \cdot x^{-1} = 1 \bmod p
        \end{equation}
      }
      \only<4>{
        Example: within $\mathbb{Z}_9$ $4 \cdot 7 = 28 = 1 \bmod 9$, so $4^{-1} = 7 \bmod 9$ \\
        But $3$ has no multiplicative inverse. Why?
      }
      \onslide<5->{
        \begin{equation}
          \tag{Existence of the inverse}
          x \in \mathbb{Z}_p, x^{-1} \in \mathbb{Z}p \iff \gcd(x, p) = 1
        \end{equation}
      }
      \only<5>{
        \begin{align}
          \notag
          gcd(p, 9): p \in \mathbb{Z}_9 & = \lbrace 1, 1, 3, 1, 1, 3, 1, 1 \rbrace \\
          \notag
          gcd(p, 7): p \in \mathbb{Z}_7 & = \lbrace 1, 1, 1, 1, 1, 1, 1 \rbrace
      \end{align}
      }
      \onslide<6>{} % Empty slide to hide examples
      \note<6>{
	This set of integers is a finite field (but only if its size is a prime power).
        Zero has no multiplicative inverse by definition, so it's not important for the existence proof.
      }
    \end{frame}

    \begin{frame}{Discrete maths\hfill2/3}
      \onslide<1->{
        \begin{equation}
          \tag{All elements in $\mathbb{Z}_p$ have an inverse if $p \in \mathbb{P}$}
          p \in \mathbb{P} \iff \bigwedge_{n \in (1..p-1)} \gcd(n, p) = 1
        \end{equation}
      }
      \onslide<2->{
        \begin{align}
          \notag
          n \in \mathbb{P} & \iff \phi(n) = n - 1 \\
          n = p \cdot q \wedge p \neq q \wedge p, q \in \mathbb{P} & \iff \phi(n) = (p - 1)(q - 1)
          \tag{Euler's Totient function}
        \end{align}
      }
      \only<3>{
        Example:
        \begin{align}
          \notag
          \phi(7) & = \vert \{ 1,2,3,4,5,6 \} \vert = 6 \\
          \notag
          \phi(9) & = \vert \{ 1,2,4,5,7,8 \} \vert = 6
        \end{align} 
      }
      \onslide<4->{
        \begin{equation}
          \tag{Primitive root}
          \bigwedge_a gcd(a, n) = 1 : g^k \equiv a \bmod n
        \end{equation}
      }
      \only<5>{
        Example:
        2 is a primitive root of 5. Successive powers of 2: $2, 4, 8, 16, 32$. Taken modulo 5: $2, 4, 3, 1, 2, \ldots$
        \note<4->{That is, if every power of g modulo n is coprime to n. Or equvalently, all are nonzero members of $\mathbb{Z}_n$}
      }
    \end{frame}

    \begin{frame}{Discrete maths\hfill3/3}
      \only<1>{
        \begin{equation}
          \tag{Exponentiation}
          {(x^a)}^b = x^{a \cdot b}
        \end{equation}
      }
      \onslide<2->{
        \begin{equation}
          \tag{Modular exponentiation}
          {(x^a)}^b \bmod p = x^{a \cdot b} \bmod p
        \end{equation}
      }
      \onslide<3->{
        \begin{align}
          \tag{Fermat's Little Theorem}
          p \in \mathbb{P}, a^p & \equiv a \bmod p \\
          \notag
          a^{p - 1} & \equiv 1 \bmod p
        \end{align}
      }
      \onslide<4->{
        \begin{align}
          \tag{When n is a product of two primes}
          n = p \cdot q &, \quad p, q \in \mathbb{P} \\
          \tag{a pair of inverses modulo totient}
          e \cdot d & = 1 \bmod \phi(n) \\
          \tag{Encoding and decoding}
          {(a^e)}^d \bmod n & = a^{e \cdot d} \bmod n =  a
        \end{align}
      \begin{flushright}
        \hyperlink{rsaproof}{\beamerbutton{Show me the proof}}
        \hyperlink{rsaezmode}{\beamerbutton{Show me an example}}
      \end{flushright}
      }
    \end{frame}

    \section{How it's made: RSA keys}

    \begin{frame}{RSA Key generation \hfill { \faPuzzlePiece }}
      \label{keygen}
      \begin{enumerate}
        \item<1-> Choose two large primes $p, q \in \mathbb{P}$
        \item<2-> Calculate modulus $\mathbf{n} = p \cdot q$
        \item<2-> Calculate totient $\phi(n) = (p - 1)(q - 1)$
        \item<3-> Pick a \textcolor{blue}{public} key $\mathbf{e} = 65537$
          \uncover<4->{or another value if $e$ divides $\phi(n)$}
        \item<5-> Find the inverse $\mathbf{d} = e^{-1} \bmod \phi(n)$.
          \uncover<6->{This is the \textcolor{red}{private} key}
      \end{enumerate}
      \onslide<7->{It is considered hard to do this inverse operation if you only know $e, n$ and not the totient. RSA's security is based on this principle.}
      \note<7->{
        65537 is used because it's has only two 1 bits, and it's very cheap to do
        modular exponentiation with it.
      }
    \end{frame}

    \begin{frame}{RSA Key distribution \hfill { \faPaperPlane }}
      \begin{itemize}
        \item Distribute $(e, n)$ freely, this is the \textcolor{blue}{public} part
        \item Keep $(d, n)$ secret, this is the \textcolor{red}{private} part
        \item For performance, other derived values are sometimes stored with the private key
      \end{itemize}
    \end{frame}

    \section{Under the hood}

    \begin{frame}[fragile]{Creating a key \hfill {\faKey}}
      Default format
      \begin{verbnobox}
    $ ssh-keygen -q -t rsa -f example_key
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
      \end{verbnobox}
      PKCS\#8 format, usable with other tools
      \begin{verbnobox}
    $ ssh-keygen -q -t rsa -f example_key -m PKCS8
      \end{verbnobox}
    \end{frame}

    \begin{frame}[fragile]{Disassembling a PKCS8 private key (RFC5208) \hfill {\faSearchPlus}}
      \begin{columns}
        \begin{column}{0.4\textwidth}
          \begin{verbnobox}[\tiny]
    $ openssl pkey -in example_key -text -noout
    RSA Private-Key: (3072 bit, 2 primes)
    modulus:
        00:a4:ae:39:33:18:67:73:82:15:2d:27:b7:4b:2b:
        92:fc:62:02:50:8b:08:2b:77:f6:e3:14:f4:24:3c:
        [trimmed]
    publicExponent: 65537 (0x10001)
    privateExponent:
        4d:97:00:c0:40:39:4d:c4:f6:71:46:0b:28:51:a9:
        [trimmed]
    prime1:
        00:d9:e6:c1:c4:16:03:4b:4b:d3:d0:0e:0a:f2:c1:
        [trimmed]
    prime2:
        00:c1:79:4e:c5:cf:60:7a:9e:d5:ea:03:ab:ed:56:
        [trimmed]
    exponent1:
        00:cc:fc:d5:aa:37:1f:30:35:8c:74:bc:d1:35:df:
        [trimmed]
    exponent2:
        00:93:c4:da:34:5d:12:7c:35:2a:7e:e4:b5:00:bd:
        [trimmed]
    coefficient:
        [trimmed]
          \end{verbnobox}
        \end{column}
        \begin{column}{0.6\textwidth}
          \begin{description}
              \small
            \item{modulus:} $n$
            \item{\textcolor{blue}{publicExponent:}} $e$
            \item{\textcolor{red}{privateExponent:}} $d$
            \item{prime1:} $p$
            \item{prime2:} $q$
            \item{exponent1:} $d \bmod (p - 1)$
            \item{exponent2:} $d \bmod (q - 1)$
            \item{coefficient:} $q^{-1} \bmod p$
          \end{description}
        \end{column}
      \end{columns}
      \note{So it actually contains the public key as well. This is sufficient to reconstruct it given the private key, but there are easier ways.}
    \end{frame}

    \begin{frame}{Disassembling a SSH public key \hfill {\faSearchPlus}}
      OpenSSH public key format: \\
      \texttt{key-type base64-encoded-data comment} \\
      \texttt{ssh-rsa AAAAAhxlsadj... somebody@oncetold.me} \\
      Let's dissect the middle part, the other two aren't interesting.
    \end{frame}
    
    \begin{frame}[fragile]{Public key internals \hfill {\faCode}}
      \hspace{-0.5cm}\begin{minipage}{\textwidth}
        \begin{multicols}{2}
        \inputminted[fontsize=\small]{ruby}{read_pubkey.rb} 
        \end{multicols}
      \end{minipage}\hspace{-0.5cm}
    \end{frame}

    \begin{frame}[fragile]{Extracting exponent and modulus \hfill { \faSearchPlus }}
      \begin{minted}{shell}
$ cut -d' ' -f2 example_key.pub | base64 -d | 
  ./read_pubkey.rb
ssh-rsa
65537 (10001)
"a4:ae:39:33:18:67:73:82:15:2d:27:b7:4b:[trimmed]"
      \end{minted}
      Exponent and modulus match!
    \end{frame}

    \begin{frame}[fragile]{Disassembling a SSH private key \hfill {\faSearchPlus}}
      The format is somewhat proprietary, but similar
      \begin{minted}[fontsize=\tiny]{text}
"openssh-key-v1"0x00    # NULL-terminated "Auth Magic" string
32-bit length, "none"   # ciphername length and string
32-bit length, "none"   # kdfname length and string
32-bit length, nil      # kdf (0 length, no kdf)
32-bit 0x01             # number of keys, hard-coded to 1 (no length)
32-bit length, sshpub   # public key in ssh format
    32-bit length, keytype
    32-bit length, pub0
    32-bit length, pub1
32-bit length for rnd+prv+comment+pad
    64-bit dummy checksum?  # a random 32-bit int, repeated
    32-bit length, keytype  # the private key (including public)
    32-bit length, pub0     # Public Key parts
    32-bit length, pub1
    32-bit length, prv0     # Private Key parts
    ...                     # (number varies by type)
    32-bit length, comment  # comment string
    padding bytes 0x010203  # pad to blocksize
      \end{minted}
      \note{The sshpub part is identical to what we just decoded from a public key}
    \end{frame}


    \begin{frame}[fragile]{Reconstructing public key from private \hfill { \faUndo }}
      \begin{minted}[fontsize=\tiny]{shell}
# Reads private key, outputs corresponding public key, SSH format
$ ssh-keygen -y -f example_key > example_key.pub
# Alternatively, read pub/priv key, export public in chosen format
$ ssh-keygen -e -m FORMAT -f example_key > example_key.pub
# RFC4716 is actually the default format for some newer installations of OpenSSH
      \end{minted}
       Does it match?
      \begin{minted}[fontsize=\tiny]{shell}
# Using PKCS8 for easy openssl compatibility
$ ssh-keygen -e -m PKCS8 -f example_key | openssl rsa -pubin -noout -text
RSA Public-Key: (3072 bit)
Modulus:
    00:a4:ae:39:33:18:67:73:82:15:2d:27:b7:4b:2b:
    [trimmed]
Exponent: 65537 (0x10001)
      \end{minted}
    \end{frame}

    \begin{frame}{Is anything actually encrypted with these keys? \hfill { \faQuestionCircle }}
      \begin{center}
        \only<1>{\Large ???}
        \only<2>{\Large No}
        \only<3>{\Large No, but actually yes, a tiny bit}
      \end{center}
    \end{frame}

    \begin{frame}[fragile]{Only for authentication (RFC4252) \hfill { \faFemale \faSignIn \faDesktop }}
      \label{keynegotiation}
      These keys aren't used for actual encryption, only for key negotiation.
      \begin{minted}[fontsize=\tiny]{text}
      byte      SSH_MSG_USERAUTH_REQUEST
      string    user name
      string    service name
      string    "publickey"
      boolean   TRUE
      string    public key algorithm name
      string    public key to be used for authentication
      --------------------------------------------------
      string    signature
      \end{minted}
      Signature is the session identifier, plus everything above it, encrypted with corresponding private key. \\
      \hyperlink{rsaezmode}{\beamerbutton{Show me how encryption works}}
      \note{This is key negotiation: after D-H session key exchange, your client offers any keys in that format. The server may reject on any reason (e.g. weak algorithm), but if the signature can be decrypted with its public key, then it's a definite match.}
    \end{frame}

    \section{Handling keys}

    \begin{frame}{Storing keys \hfill {\faHddO}}
      \begin{itemize}
        \item \texttt{\$HOME/.ssh} is the canonical location
        \item Directory should be 0700 (rw only by owner). Private keys 0600 (same for files).
        \item \texttt{ssh-agent} starts with your session, doesn't load anything unless told
        \item \texttt{ssh-add} tells it to load keys. These can be stored anywhere you want!
        \item \texttt{AddKeysToAgent} option in \texttt{ssh\_config} tells ssh to add any loaded key to agent
      \end{itemize}
    \end{frame}

    \begin{frame}{Which keys are actually used? \hfill { \faQuestionCircle }}
      \begin{center}
        \onslide<2->{\Large ALL OF THEM} \\
        \onslide<3->{\small Unless you tell ssh otherwise} \\
      \end{center}
      \note<3->{Details upcoming in a couple of screens}
    \end{frame}

    \begin{frame}[fragile]{Selecting keys used \hfill { \faEyeSlash }}
      \begin{itemize}
        \item<1-> \texttt{ssh -i keyfile}
        \item<1-> \texttt{alias sshstartup=ssh -i \~{}/keys/startup}
        \item<2-> \texttt{IdentityFile} option in \texttt{ssh\_config}, can be used many times
        \begin{minted}{apache}
Host staging.startup.eu
IdentityFile ~/keys/startup
Host production.startup.eu
IdentityFile ~/keys/startup_prod
        \end{minted}
        \item<3-> Useless for github though
      \end{itemize}
    \end{frame}

    \begin{frame}{Github (and Gitlab, Bitbucket etc) \hfill {\faGithub} {\faGitlab} {\faBitbucket}}
      \begin{itemize}
        \item<1->Authenticate you by public key (not username/email)
        \item<2->If all keys are tried, which one is accepted? \onslide<3->{ \\ Answer: whichever is presented first}

        \item<4-> \textbf{\texttt{permission denied (publickey)}}
        \item<5->Juggling keys with \texttt{ssh-add -D} and \texttt{ssh-add somekey} is inconvenient \\ \footnotesize{also your ssh-agent may disagree, \emph{gnome-keyring} does}
        \item<6->So is adding aliases for github in \texttt{\~{}/.ssh/config}
      \end{itemize}
    \end{frame}

    \begin{frame}[fragile]{\texttt{core.sshCommand} \hfill {\faTerminal}}
      \begin{minted}[fontsize=\small]{shell}
# To clone using a specific key
GIT_SSH_COMMAND="ssh -i ~/keys/startup" \
git clone git@github.com:some/repo
# then inside existing repo
git config --add core.sshCommand "ssh -i ~/keys/startup"
      \end{minted}
      Since Git 2.3.0 (released in 2015). Earlier solutions used \texttt{GIT\_SSH} wit a wrapper script, now obsolete.
    \end{frame}

    \begin{frame}{Agent comes first \hfill { \faUserSecret }}
      Keys are offered in that order:
      \begin{enumerate}
        \item \emph{All} keys from ssh-agent, in order of loading
        \item Key named with \texttt{ssh -i identity} argument, if any
        \item For \textbf{every} matching \texttt{Host} or \texttt{Match} directive, all keys specified with \texttt{IdentityFile}.
        \item Standard keys in \texttt{\~{}/.ssh}: \texttt{id\_rsa, id\_dsa, id\_ed25519} etc \emph{if nothing matched}
	    \end{enumerate}
      \note<1->{Especially if you have wildcard rules in sshconfig, or just list IdentityFiles without restricting them}
    \end{frame}

    \begin{frame}{Too many keys \hfill { \faExclamationCircle }}
      \begin{itemize}
        \item \texttt{MaxAuthTries 6} is the default
        \item Keys, password, Challenge-Response auths all count towards that limit
        \item Can't login if limit exhausted
        \item<2-> \texttt{IdentitiesOnly yes}
      \end{itemize}
      \note<1->{After that many keys have been tried and failed, the server will disconnect you}
    \end{frame}

    \begin{frame}{With \texttt{IdentitiesOnly} \hfill { \faGear }}
      Keys are offered in that order:
      \begin{enumerate}
        \item Keys from ssh-agent \textbf{that would match points 2 or 3}
        \item Key named with \texttt{ssh -i identity} argument, if any
        \item For matching \texttt{Host} or \texttt{Match} directives, keys specified with \texttt{IdentityFile} \emph{unless already offered by agent}
        \item Standard keys in \texttt{\~{}/.ssh}: \texttt{id\_rsa, id\_dsa, id\_ed25519} etc \emph{if nothing matched}
	    \end{enumerate}
      \note<1->{Especially if you have wildcard rules in sshconfig, or just list IdentityFiles without restricting them}
    \end{frame}


    \begin{frame}[fragile]{Public keys are \ldots public \hfill { \faPaw }}
        \begin{center}
          \texttt{ssh whoami.filippo.io}
        \end{center}
    \end{frame}
    \begin{frame}[fragile]{Public keys are \ldots public \hfill { \faPaw }}
      Only for the paranoid
      \begin{minted}{apache}
# at the end of your ssh_config
Host *
PubkeyAuthentication No
      \end{minted}
      because now you have to whitelist every new host you connect to
    \end{frame}

    \begin{frame}{Private keys are security credentials! \hfill {\faBullseye}}
      \begin{itemize}
        \item NEVER store them in VCS (\texttt{git}), risking accidental exposure
        \item Passphrase protect for better security
        \item Priority target for hackers
      \end{itemize}
    \end{frame}

    \begin{frame}{Reusing and replicating keys \hfill {\faBan} {\faCopy} {\faArrowRight} {\faPaste}}
      Tempting and convenient, but try not to.
      \begin{itemize}
        \item Can't identify leak source
        \item Generate distinct keys for all devices you use
        \item When device lost or stolen, revoke ASAP
        \item When someone quits the project, revoke ASAP
        \item \texttt{RevokedKeys} option for \texttt{sshd} and Key Revocation Lists
      \end{itemize}
      Avoid reusing keys in different security contexts (between separate projects/employers).
    \end{frame}

    \begin{frame}{Authorized keys \hfill {\faUnlock}}
      \begin{itemize}
        \item Stored on the host you're connecting to
        \item \textcolor{blue}{public keys} only.
        \item \texttt{ssh-copy-id} copies your public keys to any host you can log in with a password or another key
        \item \texttt{ssh-copy-id -i keyfile user@host} will install only this one key
        \item This is where \texttt{sshd} grabs keys from during authentication phase
              \hyperlink{keynegotiation}{\beamerbutton{Details}}
      \end{itemize}
    \end{frame}

    \begin{frame}{Storing keys \textbf{outside} a filesystem \hfill {\faUsb}}
      \begin{columns}
        \column{0.3\textwidth}
          Yubikey, PIVKey \\
          \includegraphics[width=\textwidth]{yubikey-5.png} 
        \column{0.7\textwidth}
        \begin{itemize}
          \item ARM-based CPU and flash memory
          \item Durable construction
          \item Impossible to extract \textcolor{red}{private key} once loaded
          \item Performs crypto (signing) on-board
          \item \texttt{ssh-add -s /usr/lib64/opensc-pkcs11.so} tells agent to use it
          \item \texttt{ssh-keygen -D} extracts \textcolor{blue}{public key}
          \item Setup nontrivial, but usage a breeze
          \item \$50 apiece, not great not terrible
        \end{itemize}
      \end{columns}
      \note{USB-C, NFC, even a lightning connector version exists}
    \end{frame}

    \begin{frame}{Tools summary \hfill { \faWrench }}
      \begin{itemize}
        \item \texttt{ssh-keygen} generate and manage keyfiles
        \item \texttt{ssh-agent} securely caches keys in memory (useful with passphrases)
        \item \texttt{ssh-add} talks to agent
        \item \texttt{ssh-copy-id} helps install public keys
        \item \texttt{sftp, scp} move files between hosts
        \item \texttt{rsync} moves files, synchronizes directories and more
      \end{itemize}
    \end{frame}

  \section{Extras}

  \begin{frame}{Diffie-Hellman key exchange (RFC4419) \hfill { \faHandRockO \faHandPaperO \faHandScissorsO}}\label{dhex}
  \begin{enumerate}
    \item<1-> Alice and Bob publicly agree to use a modulus $p = 23$ and base $g = 5$ (which is a primitive root of 23).
    \item<2-> Alice chooses a secret integer $a = 4$, then sends Bob $A = g^a \bmod p = 5^4 \bmod 23 = 4$
    \item<2-> Bob chooses a secret integer $b = 3$, then sends Alice $B = g^b \bmod p = 5^3 \bmod 23 = 10$
    \item<3-> Alice computes $s = B^a \bmod p = 10^4 \bmod 23 = 18$
    \item<3-> Bob computes $s = A^b \bmod p = 4^3 \bmod 23 = 18$
    \item<4-> Alice and Bob now share a secret (the number 18).
    \item<5-> They switch to a symmetric cipher where key is somehow derived from 18.
  \end{enumerate}
  \end{frame}

  \begin{frame}{Proof that RSA en/decryption works\hfill1/3}\label{rsaproof}
    \begin{align}
      \tag{Encryption}
      c & = m^e \bmod n \\
      \tag{Decryption}
      c^d \bmod n & = (m^e)^d \bmod n = m \bmod n \\
      \notag
      e \cdot d & = 1 \bmod \phi(n) \\
      \tag{$e$ and $d$ are inverses in $\mathbb{Z}_{\phi(n)}$}
      e \cdot d & = k \cdot \phi(n) + 1 \\
      \notag
      (m^e)^d \bmod n = m^{e \cdot d} & = m^{k \cdot \phi(n) + 1} \bmod n \\
      \tag{Fact about $\phi(n)$}
      \phi(n) & = (p - 1)(q - 1) \\
      \notag
      m^{k \cdot \phi(n) + 1} \bmod n & = m^{k \cdot (p-1)(q-1) + 1} \bmod n
    \end{align}
  \end{frame}

  \begin{frame}{Proof that RSA en/decryption works\hfill2/3}
    \begin{align}
      \notag
      m^{k \cdot \phi(n) + 1} & = m^{k \cdot (p-1)(q-1) + 1} = {(m^{p-1})}^{k \cdot (q-1)} \cdot m \\
      \tag{Fermat's Little Theorem}
      m^{p-1} & = 1 \bmod p \\
      \notag
      {(m^{p-1})}^{k \cdot (q-1)} \cdot m \bmod n & = (1 \bmod p)^{k \cdot (q-1)} \bmod n \\
      \tag{therefore $p$ divides $n$}
      n & = p \cdot q \\
      \tag{Chinese Remainder Theorem}
      (1 \bmod p)^{k \cdot (q-1)} \bmod n & = 1 \cdot m \bmod p \\
      \notag
      m^{e \cdot d} \bmod n & = m \bmod p
    \end{align}
  \end{frame}

  \begin{frame}{Proof that RSA en/decryption works\hfill3/3}
    \begin{align}
      \notag
      m^{e \cdot d} \bmod n & = m \bmod p \\
      \tag{Previous page also holds for $q$}
      m^{e \cdot d} \bmod n & = m \bmod q \\
      \notag
      n & = p \cdot q \\
      \notag
      x = m \bmod p = m \bmod q & \iff x = m \bmod p \cdot q \\
      \notag
      m^{e \cdot d} \bmod n & = m \bmod n \quad \qedsymbol
    \end{align}
    \note{For the collapsing of modulos, try 17, which is 2 mod 3 and also 2 mod 5. Therefore it is 2 mod 15. Or any number that is mod 1 to both 2 and 5 is of the form 10k + 1}
  \end{frame}

  \begin{frame}{Easy-mode RSA \hfill 1/2}\label{rsaezmode}
    \onslide<1->{
      \begin{align}\notag
        p & = 11, q = 13, n = p \cdot q = 143 \\
        \notag
        \phi(n) & = (p - 1)(q - 1) = 120
      \end{align}
    }
    \onslide<2->{
      \begin{align}
        \tag{Pick a public key}
        e & = 7, \quad gcd(e, \phi(n)) = 1 \\
        \tag{Private key}
        d & = 103
      \end{align}
    }
    \onslide<3->{
      \begin{align}
        \tag{Matching keys}
        e \cdot d \bmod \phi(n) & = 7 \cdot 103 \bmod 120 \\
        \notag
        & = 721 \bmod 120 = (6 \cdot 120) + 1 \bmod 120 \\
        \notag
        & = 1 \bmod 120
      \end{align}
    }
  \end{frame}

  \begin{frame}{Easy-mode RSA \hfill 2/2}
    \onslide<1->{
      \begin{equation}\tag{Our secret message}
        m = 9 
      \end{equation}
    }
    \onslide<2->{
      \begin{equation}
        \tag{Encrypting}
        m^e \bmod n = 9^7 \bmod 143 = 48 = c \\
      \end{equation}
    }
    \onslide<3->{
      \begin{equation}
        \tag{Decrypting}
        c^d \bmod n = 48^{103} \bmod 143 = 9 = m
      \end{equation}
      \begin{flushright}
        \hyperlink{rsaproof}{\beamerbutton{Why does this work again?}}
      \end{flushright}
    }
  \end{frame}

  \begin{frame}{Real-world RSA \hfill { \faShip }}
    \begin{itemize}
      \item We don't encode each byte separately
      \item Deterministic and prone to known-text attacks
      \item Needs secure padding (with \underline{\href{https://en.wikipedia.org/wiki/Optimal\_asymmetric\_encryption\_padding}{OAEP}})
      \item Keys are 3072 or more bits long
    \end{itemize}
  \end{frame}

  \begin{frame}{\acs{PAM}: \acl{PAM} \hfill { \faShield }}
    \label{pam}
    \begin{itemize}
      \item Not part of SSH, but controls login process
      \item Unix standard (Linux, OSX, BSD, Solaris, etc)
      \item Also used to enforce \texttt{sudo} access
      \item Defines scopes: \textbf{account}, \textbf{auth}entication, \textbf{password}, \textbf{session}
      \item Each scope runs a number of \textbf{modules}
      \item Boring and secure
    \end{itemize}
  \end{frame}

  \begin{frame}{\acl{PAM} showcase \hfill { \faMagic }}
     \begin{itemize}
       \item \underline{\href{https://github.com/wumb0/pam\_konami/blob/master/pam\_konami.c}{pam\_konami}}: 
	       Input \keys{\arrowkeyup} \keys{\arrowkeyup} \keys{\arrowkeydown} \keys{\arrowkeydown} \keys{\arrowkeyleft} \keys{\arrowkeyright} \keys{\arrowkeyleft} \keys{\arrowkeyright} \keys{B} \keys{A} \keys{\return} to login
       \item \underline{\href{https://gitlab.com/k3rni/pam-konami}{pam-konami}}:
	       same, but requires {\faGamepad} for input, by me
       \item \underline{\href{https://github.com/google/google-authenticator-libpam}{libpam-google-authenticator}}: require OTP to login
       \item \underline{\href{https://github.com/Yubico/yubico-pam}{yubico-pam}}: require (or replace password with) Yubikey presence
       \item \underline{\href{https://github.com/Yubico/pam-u2f}{pam-u2f}}: require U2F token
       \item \underline{\href{https://github.com/Reflejo/pam-touchID}{pam-touchID (\faApple)}}
             or \underline{\href{https://gitlab.freedesktop.org/libfprint/libfprint}{pam\_fprintd}}: require fingerprints
       \item \underline{\href{https://github.com/nullpo-head/WSL-Hello-sudo}{pam\_wsl\_hello (\faWindows WSL)}}
             or \underline{\href{https://github.com/boltgolt/howdy}{howdy}}: require your face
     \end{itemize}
  \end{frame}

  \section{Demo}
  
  \begin{frame}{\acs{PAM} demo \hfill { \faTerminal }}
    \begin{center}
      {\Large Demo Time?}
    \end{center}
  \end{frame}

  \section{References}

  \begin{frame}{Documentation \hfill { \faBookmark }}
    \begin{itemize}
      \item RFC 4252: The Secure Shell
      \item RFC 4253: SSH Transport
      \item RFC 4716: SSH Public Key Format
      \item RFC 5208: PKCS8 Private Key Syntax Spec
      \item RFC 8017: RSA Cryptography Spec
      \item Manpages: \texttt{ssh, ssh-add, ssh-keygen ssh\_config, sshd, sshd\_config}
    \end{itemize}
  \end{frame}
  \begin{frame}{Reading \hfill { \faGlobe }}
    \begin{itemize}
      \item \underline{\href{https://stribika.github.io/2015/01/04/secure-secure-shell.html}{Secure Secure Shell, by Stribika}}
      \item \underline{\href{http://doctrina.org/How-RSA-Works-With-Examples.html}{How RSA works, by Doctrina}}
      \item \underline{\href{http://doctrina.org/Why-RSA-Works-Three-Fundamental-Questions-Answered.html}{Why RSA works, by Doctrina}}
      \item \underline{\href{https://coolaj86.com/articles/the-ssh-public-key-format/}{SSH Public Key format, by AJ O'Neal}}
      \item \underline{\href{https://utcc.utoronto.ca/~cks/space/blog/sysadmin/SSHIdentitiesOffered}{SSH Identities Offered, by Chris Siebenmann}}
      \item \underline{\href{https://blog.rebased.pl/2020/02/10/ssh-key-internals.html}{On SSH keys, \textit{by me}}}
    \end{itemize}
  \end{frame}
  \begin{frame}{Books \hfill {\faBook}}
	  \begin{itemize}
      \item Andrew S. Tannenbaum, David J. Wetherall \textit{Computer Networks}, 5th ed., Chapter 8.3: Public-Key Algorithms
    \end{itemize}
  \end{frame}

  \begin{frame}{EOF \hfill { \faStopCircle }}
    \begin{center}
      \begin{tabular}{r l}
        me & Krzysztof Zych, not a crypto expert \\
        \faTwitter & {@\_k3rni} \\
        working for & \raisebox{-0.5ex}{\includegraphics[width=3cm]{\logofilename}} \\
        this presentation & \url{gitlab.com/k3rni/on-ssh-keys}
      \end{tabular}
    \end{center}
  \end{frame}
\end{document}
