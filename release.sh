#! /bin/bash

GITLAB="gitlab -c gitlab.cfg"

project_id() {
  $GITLAB project list --owned 1 --search on-ssh-keys | cut -d' ' -f2 | head -1
}

latest_tag() {
  git tag | tail -1
}

prompt_release_name() {
  read -p "Release name: "
}

echo Using tag: $(latest_tag)
read -p "Release name: " RN
read -p "Description: " DESC

$GITLAB project-release create --project-id $(project_id) --tag-name $(latest_tag) --name "$RN" --description "$DESC"
