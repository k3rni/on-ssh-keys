#! /usr/bin/env ruby
def read_str(io=STDIN)
  size, = io.read(4)
    .unpack('L>')
  io.read(size)
end
def read_num(io=STDIN)
  bytes = read_str(io)
  # Parse long integer
  s = bytes.each_char
    .inject(0) { |a, b|
    (a << 8) | b.ord }
  if bytes.size > 4
    # Format nicely
    s.to_s(16).each_char
      .each_slice(2)
      .map(&:join).join(':')
  else
    "#{s} (#{s.to_s(16)})"
  end
end

key_type = read_str
exponent = read_num
modulus = read_num
puts key_type, exponent,
     modulus.inspect

