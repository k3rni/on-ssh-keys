# On SSH keys

Those are slides for my presentation about SSH keys and some aspects of PAM.

# Where are the PDFs?

## [Browse artifacts](../-/jobs/artifacts/master/browse?job=compile)

The files are built automatically in Gitlab CI, use link above to browse most recent build. Files are available in dark and light variants, for 4:3 (beamer traditional) and 16:9 (modern) aspect ratios.

# Building the files yourself

Compiling the PDF files requires a number of LaTeX packages:

    apt install lualatex texlive texlive-latex-extra texlive-fonts-extra texlive-pictures

