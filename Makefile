# LATEX=lualatex -shell-escape
LATEX=latexmk -lualatex -latexoption=-shell-escape --output-directory=build
LIGHT=light43.pdf light169.pdf
DARK=dark43.pdf dark169.pdf

all: build $(LIGHT) $(DARK) notes.pdf

build:
	mkdir -p $@

%.tex: content.tex build
	touch -r $< $@

%.pdf: %.tex
	$(LATEX) --output-directory=build $^
	mv build/$@ .
